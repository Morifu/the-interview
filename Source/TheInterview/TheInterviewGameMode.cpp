// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TheInterview.h"
#include "TheInterviewGameMode.h"
#include "TheInterviewHUD.h"
#include "TheInterviewCharacter.h"

ATheInterviewGameMode::ATheInterviewGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATheInterviewHUD::StaticClass();
}
