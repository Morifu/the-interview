// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "TheInterviewGameMode.generated.h"

UCLASS(minimalapi)
class ATheInterviewGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATheInterviewGameMode();
};



