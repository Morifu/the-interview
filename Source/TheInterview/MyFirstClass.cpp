// Fill out your copyright notice in the Description page of Project Settings.

#include "TheInterview.h"
#include "MyFirstClass.h"
#include "Utilities/Templates/BasicContainers.h"
#include <iostream>
using namespace ELogVerbosity;
// Sets default values
AMyFirstClass::AMyFirstClass()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyFirstClass::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AMyFirstClass::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

