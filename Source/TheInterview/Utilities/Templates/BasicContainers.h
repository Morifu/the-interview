
#pragma once
#include <assert.h>
#include <algorithm>
namespace Interview {
    
    /**
     * \brief Template array class for implementation example purposes 
     * \tparam T type object
     */
    template<class T>
    class InterviewArray {
    public:
        explicit InterviewArray(int size, int growSize = 1):
        m_array(NULL),m_maxSize(0), m_growBy(0), m_numElements(0){
            if (size) {
                m_maxSize = size;
                m_array = new T[m_maxSize];
                memset(m_array, 0, sizeof(T)*m_maxSize);
                m_growBy = (growSize > 0) ? growSize : 0;
            }
        }

        virtual ~InterviewArray() {
            if (m_array != NULL) {
                delete[] m_array;
                m_array = NULL;
            }
        }

        void push(T val, bool autosort = false) {
            assert(m_array != NULL);
            if (m_numElements > m_maxSize) {
                Expand();
            }
            int i = m_numElements;

            if(autosort) {
                for (i = 0; i < m_numElements; i++) {
                    if (m_array[i] > val) {
                        break;
                    }
                }

                for (int k = m_numElements; k > i; k--) {
                    m_array[k] = m_array[k - 1];
                }
            }

            m_array[i] = val;
            m_numElements++;
        }

        void pop() {
            if(m_numElements > 0) {
                m_numElements--;
            }
        }

        void remove(int index) {
            assert(m_array != NULL);

            if(index >= m_maxSize) {
                return;
            }

            for(int k = index; k < m_maxSize - 1; k++) {
                m_array[k] = m_array[k + 1];
            }

            m_maxSize--;

            if(m_numElements >= m_maxSize) {
                m_numElements = m_maxSize-1;
            }
        }

        T& operator[](int index){
            assert(m_array != NULL && index <= m_numElements);
            return m_array[index];
        }

        int search(T key) {
            if (!m_array) {
                return -1;
            }

            int lowerBound = 0;
            int upperBound = m_numElements - 1;
            int current = 0;

            while (true) {
                current = (lowerBound + upperBound) >> 1;

                if(m_array[current] == key) {
                    return current;
                } else if(lowerBound > upperBound) {
                    return -1;
                } else {
                    if(m_array[current] < key) {
                        lowerBound = current + 1;
                    } else {
                        upperBound = current - 1;
                    }
                }
            }
        }
        
        /**
         * \brief Method for sorting the array using Bubble Sort algorithm
         * the slowest of them all with O(n2)
         */
        void BubbleSort() {
            assert(m_array != NULL);

            for(int k = m_numElements -1; k > 0; k--) {
                for(int i = 0; i < k; i++) {
                    if(m_array[i] > m_array[i+1]) {
                        T temp = m_array[i];
                        m_array[i] = m_array[i + 1];
                        m_array[i + 1] = temp;
                    }
                }
            }
        }

        /**
        * \brief Method for sorting the array using Selection Sort algorithm
        * it's still slow with complexity of O(n2)
        */
        void SelectionSort() {
            assert(m_array != NULL);

            int min = 0;

            for (int k = 0; k < m_numElements - 1; k++) {
                min = k;
                for(int i = k+1; i < m_numElements; i++) {
                    if(m_array[i] < m_array[min]) {
                        min = i;
                    }
                }

                if(m_array[k] > m_array[min]) {
                    T temp = m_array[k];
                    m_array[k] = m_array[min];
                    m_array[min] = temp;
                }
            }
        }

        /**
        * \brief Method for sorting the array using Insertion Sort algorithm
        * it Runs in O(n2) but still faster than selection sort
        */
        void InsertionSort() {
            assert(m_array != NULL);

            int i = 0;

            for(int k = 1; k < m_numElements; k++) {
                T temp = m_array[k];
                i = k;

                while(i > 0 && m_array[i-1] >= temp) {
                    m_array[i] = m_array[i - 1];
                    i--;
                }
                m_array[i] = temp;
            }
        }

        /**
        * \brief Method for sorting the array using Merge Sort algorithm
        * it Runs in O(n2) but still faster than selection sort
        */
        void MergeSort() {
            assert(m_array != NULL);

            T *tempArray = new T[m_numElements];
            assert(tempArray != NULL);

            MergeSort(tempArray, 0, m_numElements - 1);
            delete[] tempArray;
        }

        void clear() { m_numElements = 0; }
        int GetSize() const { return m_numElements; }
        int GetMaxSize() const { return m_maxSize; }
        int GetGrowSize() const { return m_growBy; }

        void SetGrowSize( int val ) {
            assert(val >= 0);
            m_growBy = val;
        }
    private:
        T* m_array;
        int m_maxSize;
        int m_growBy;
        int m_numElements;

        void MergeSort(T *tempArray, int lowerBound, int upperBound) {
            if(lowerBound == upperBound) {
                return;
            }

            int mid = (lowerBound + upperBound) >> 1;

            MergeSort(tempArray, lowerBound, mid);
            MergeSort(tempArray, mid + 1, upperBound);

            Merge(tempArray, lowerBound, mid + 1, upperBound);
        }

        void Merge(T *tempArray, int low, int mid, int upper) {
            int tempLow = low, tempMid = mid - 1;
            int index = 0;

            while(low <= tempMid && mid <= upper) {
                if(m_array[low] < m_array[mid]) {
                    tempArray[index++] = m_array[low++];
                } else {
                    tempArray[index++] = m_array[mid++];
                }
            }

            while(low <= tempMid) {
                tempArray[index++] = m_array[low++];
            }

            while(mid <= upper) {
                tempArray[index++] = m_array[mid++];
            }

            for(int i = 0; i < upper - tempLow + 1; i++) {
                m_array[tempLow + 1] = tempArray[i];
            }
        }

        void Expand() {
            if(m_growBy <= 0) {
                return;
            }
            T* temp = new T[m_maxSize + m_growBy];
            assert(temp != NULL);
            memcpy(temp, m_array, sizeof(T)*m_maxSize);
            delete[] m_array;
            m_array = temp;
            m_maxSize += m_growBy;
        }
    };

    
    /**
    * \brief Template linked list node class for implementation example purposes
    * \tparam T type object
    */
    template<typename T> class InterviewLinkIterator;
    template<typename T> class InterviewLinkList;

    template<typename T>
    class InterviewLinkNode {
        friend class InterviewLinkIterator<T>;
        friend class InterviewLinkList<T>;

    private:
        InterviewLinkNode() : m_next(NULL), m_previous(NULL){}
        T m_data;
        InterviewLinkNode *m_next;
        InterviewLinkNode *m_previous;
    };

    /**
    * \brief Iterator class for template linked list
    * \tparam T type object
    */
    template<typename T>
    class InterviewLinkIterator {
    public:
        InterviewLinkIterator() {
            m_node = NULL;
        }
        ~InterviewLinkIterator(){}

        bool isValid() {
            return m_node != NULL;
        }

        void operator=(InterviewLinkNode<T> *node){
            m_node = node;
        }

        T& operator*(){
            assert(m_node != NULL);
            return m_node->m_data;
        }

        void operator++(){
            assert(m_node != NULL);
            m_node = m_node->m_next;
        }

        void operator--(){
            assert(m_node != NULL);
            m_node = m_node->m_previous;
        }

        void operator--(int) {
            assert(m_node != NULL);
            m_node = m_node->m_previous;
        }

        void operator++(int){
            assert(m_node != NULL);
            m_node = m_node->m_next;
        }

        bool operator!=(InterviewLinkNode<T> *node){
            return (m_node != node);
        }

        bool operator==(InterviewLinkNode<T> *node){
            return (m_node == node);
        }

    private:
        InterviewLinkNode<T> *m_node;
    };

    /**
    * \brief Template linked list class for implementation example purposes
    * \tparam T type object
    */
    template<typename T>
    class InterviewLinkList {
    public:
        InterviewLinkList() : m_size(0), m_root(0), m_lastNode(0){}

        ~InterviewLinkList() {
            while(m_root != NULL) {
                Pop();
            }
        }

        InterviewLinkNode<T> *Begin() {
            assert(m_root != NULL);
            return m_root;
        }

        InterviewLinkNode<T> *End() {
            return m_lastNode;
        }

        void Push(T newData) {
            InterviewLinkNode<T> *node = new InterviewLinkNode<T>;

            assert(node != NULL);
            node->m_data = newData;
            node->m_next = NULL;
            node->m_previous = NULL;

            if(m_lastNode != NULL) {
                m_lastNode->m_next = node;
                node->m_previous = m_lastNode;
            } else {
                m_root = node;
            }

            m_lastNode = node;

            m_size++;
        }

        void Push_Front(T newData) {
            InterviewLinkNode<T> *node = new InterviewLinkNode<T>;

            assert(node != NULL);

            node->m_data = newData;
            node->m_next = NULL;
            node->m_previous = NULL;

            if(m_root != NULL) {
                node->m_next = m_root;
                m_root->m_previous = node;
                m_root = node;
            } else {
                m_root = node;
                m_lastNode = node;
            }
            m_size++;
        }

        void Pop() {
            assert(m_root != NULL);

            if(m_root->m_next == NULL) {
                delete m_root;
                m_root = NULL;
            } else {
                InterviewLinkNode<T> *prevNode = m_lastNode->m_previous;

                prevNode->m_next = NULL;
                delete m_lastNode;
                m_lastNode = prevNode;
            }
            m_size = (m_size == 0 ? m_size : m_size - 1);
        }

        void Pop_Front() {
            assert(m_root != NULL);

            InterviewLinkNode<T> *temp = m_root;
            m_root = m_root->m_next;

            if(m_root != NULL) {
                m_root->m_previous = NULL;
            }
            delete temp;

            m_size = (m_size == 0 ? m_size : m_size - 1);
        }

        void Insert_Before(InterviewLinkIterator<T> &it, T newData) {
            assert(it.m_node != NULL);

            InterviewLinkNode<T> *node = new InterviewLinkNode<T>;

            assert(node != NULL);
            node->m_data = newData;
            node->m_next = it.m_node;
            node->m_previous = it.m_node->m_previous;

            if(node->m_previous != NULL) {
                node->m_previous->m_next = node;
            }

            it.m_node->m_previous = node;

            if(it.m_node == m_root) {
                m_root = node;
            }
            m_size++;
        }

        void Insert_After(InterviewLinkIterator<T> &it, T newData) {
            assert(it.m_node != NULL);

            InterviewLinkNode<T> *node = new InterviewLinkNode<T>;

            assert(node != NULL);
            node->m_data = newData;
            node->m_next = it.m_node->m_next;
            node->m_previous = it.m_node;

            if (node->m_next != NULL) {
                node->m_next->m_previous = node;
            }

            it.m_node->m_next = node;

            if (it.m_node == m_lastNode) {
                m_lastNode = node;
            }
            m_size++;
        }

        int GetSize() const {
            return m_size;
        }

    private:
        int m_size;
        InterviewLinkNode<T> *m_root;
        InterviewLinkNode<T> *m_lastNode;
    };

    /**
    * \brief Template stack class for implementation example purposes
    * \tparam T type object
    */
    template<typename T>
    class InterviewStack {
    public:
        InterviewStack() {}
        ~InterviewStack() {}

        void push(T val) {
            m_container.Push(val);
        }

        void pop() {
            m_container.Pop();
        }

        const T& top() {
            InterviewLinkIterator<T> it = m_container.End();
            return *it;
        }

        int GetSize() const { return m_container.GetSize(); }
        int isEmpty() const { return (m_container.GetSize() == 0); }
    private:
        InterviewLinkList<T> m_container;
    };

    /**
    * \brief Template queue class for implementation example purposes
    * \tparam T type object
    */
    template<typename T>
    class InterviewQueue {
    public:
        InterviewQueue(int size) {
            assert(size > 0);
            m_size = size;
        }
        ~InterviewQueue(){}

        void push_front(T val) {
            if(m_elements.GetSize() < m_size) {
                m_elements.Push_Front(val);
            }
        }

        void push_back() {
            if(m_elements.GetSize() < m_size) {
                m_elements.Push();
            }
        }

        void pop_front() {
            m_elements.Pop();
        }

        void pop_back() {
            m_elements.Pop_Front();
        }

        const T& front() {
            InterviewLinkIterator<T> it = m_elements.End();
            return *it;
        }

        const T& back() {
            InterviewLinkIterator<T> it = m_elements.Begin();
            return *it;
        }

        int GetSize() { return m_elements.GetSize(); }
        int GetMaxSize() const { return m_size; }
        bool isEmpty() { return (m_elements.GetSize() == 0); }

        void Resize(int size) {
            assert(size > 0);
            m_size = size;
        }
    private:
        InterviewLinkList<T> m_elements;
        int m_size;
    };

    /**
    * \brief Template priority queue class for implementation example purposes
    * \tparam T type object
    */
    template<typename T, typename CMP>
    class InterviewPriorityQueue {
    public:
        InterviewPriorityQueue(int size) {
            assert(size > 0);
            m_size = size;
        }
        ~InterviewPriorityQueue() {}

        void push(T val) {
            assert(m_elements.GetSize() < m_size);

            if (m_elements.GetSize() == 0) {
                m_elements.Push(val);
            } else {
                InterviewLinkIterator<T> it = m_elements.Begin();

                CMP cmp;

                while(it.isValid()) {
                    if (cmp(val, *it)) { break; }
                    it++;
                }

                if(it.isValid()) {
                    m_elements.Insert_Before(it, val);
                } else {
                    m_elements.Push(val);
                }
            }
        }

        void pop() {
            m_elements.Pop_Front();
        }

        const T& front() {
            InterviewLinkIterator<T> it = m_elements.Begin();
            return *it;
        }

        const T& back() {
            InterviewLinkIterator<T> it = m_elements.End();
            return *it;
        }

        int GetSize() { return m_elements.GetSize(); }
        int GetMaxSize() const { return m_size; }
        bool isEmpty() { return (m_elements.GetSize() == 0); }

        void Resize(int size) {
            assert(size > 0);
            m_size = size;
        }
    private:
        InterviewLinkList<T> m_elements;
        int m_size;
    };
}